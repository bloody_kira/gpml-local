export const themeList = [
	{
		icon: './gov.png',
		title: 'EnVIRONMENT & BIOTA IMPACT',
		count: 57
	},
	{
		icon: './gov.png',
		title: 'SOCIO-ECONOMIC IMPACT',
		count: 21
	},
	{
		icon: './gov.png',
		title: 'Lifecycle & SAFE CIRCULARITY',
		count: 85
	},
	{
		icon: './gov.png',
		title: 'GOVERNANCE',
		count: 16
	},
	{
		icon: './tec.png',
		title: 'TECHNOLOGY & INNOVATION',
		count: 117
	},
	{
		icon: './cap.png',
		title: 'CAPACITY BUILDING',
		count: 124
	},
	{
		icon: './fin.png',
		title: 'FINANCING',
		count: 8
	},
];