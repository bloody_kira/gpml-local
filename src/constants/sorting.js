export const sortOptionsData = [
	{
		key: '', //score desc
		displayName: 'Relevance',
	},
	{
		key: 'title_string asc',
		displayName: 'Name ascending',
	},
	{
		key: 'title_string desc',
		displayName: 'Name descending',
	},
	{
		key: 'metadata_modified desc',
		displayName: 'Last Modified',
	},
];

export const dataSetPublishedStatuses = {
	ALL: 'All',
	PENDING: 'Pending',
	PUBLISHED: 'Published',
	DECLINED: 'Declined',
};