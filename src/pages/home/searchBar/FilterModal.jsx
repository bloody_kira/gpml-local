import React from 'react';
import styled from 'styled-components';
import { Select, Tabs, Popover } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';
import ThemeItem from './ThemeItem';
import './style.css';
import { categories, categoryIcons, transnationalOrganizations } from '../../../constants/dataSetForm';
import media from '../../../helpers/media'


const { Option } = Select;
const { TabPane } = Tabs;

const Container = styled.div`
	width: 404px;
	box-sizing: border-box;
	padding-bottom: 26px;
	max-height: 80vh;
	overflow: auto;

	${media.mobile`
		width: 100%;
  	`};
`;

const StyledTabs = styled(Tabs)`
	border: 1px solid #d7e6ee;

	& .ant-tabs-nav-list {
		width: 100% !important;
	}

	& .ant-tabs-tab {
		background-color: #00000000 !important;
		color: #036799 !important; 
		width: 100% !important;
		border: 0px !important;
	}

	& .ant-tabs-tab-active {
		background-color: #fff !important;
	}

	& .ant-tabs-tab-btn {
		color: #036799 !important;
	}

	& .ant-tabs-nav-operations {
		display: none;
	}
`;


const StyledTabPane = styled(TabPane)`
	width: 100%;
	padding: 10px;
`;

const StyledSelect = styled(Select)`
	width: 100%;
	border-radius: 8px;

	& .ant-select-selector {
		background: ${props => props.$isMultiple ? '#EDF2F7' : '#FFF'} !important;
	}

	& .ant-select-selection-placeholder {
		color: #A5B0C9 !important;
	}

	& .ant-select-selection-item {
		background-color: white !important;
	}
`;

const FormContainer = styled.div`
	margin: 25px 0px;
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
`;

const FormLabel = styled.span`
	font-style: normal;
	font-weight: normal;
	font-size: 18px;
	margin-bottom: 4px;
	display: block;
	line-height: 25px;
	color: #000000;
`;

const ThemeList = styled.div`
	margin-top: 20px;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
`;

const LabelDefaultValue = styled.div`
	font-style: italic;
`;

const StyledInfoIcon = styled(InfoCircleOutlined)`
	color: #036799;
	margin-left: 5px;
`;

const PopoverContainer = styled.div`
	max-height: 50vh;
    padding: 0 12px;
	width: 280px;
    overflow: auto;

	/* width */
	&::-webkit-scrollbar {
		width: 3px;
	}

	/* Track */
	&::-webkit-scrollbar-track {
		background: #f1f1f1; 
	}
	
	/* Handle */
	&::-webkit-scrollbar-thumb {
		background: #346896; 
	}

	/* Handle on hover */
	&::-webkit-scrollbar-thumb:hover {
		background: #1890ff;
	}
`;

const FilterModal = ({ filterData, setSearchFilterData, facets }) => {
	function renderCountrySelector() {
		const options = facets.countries.map(({ count, name }) => <Option key={name}>{`${name} (${count})`}</Option>).reverse();

		function handleChange(selectedList) {
			setSearchFilterData((prevState) => ({
				...prevState,
				countries: selectedList,
			}))
		}

		function handleTabChange() {
			setSearchFilterData((prevState) => ({
				...prevState,
				countries: [],
				multiCountry: []
			}))
		}

		return (
			<FormContainer>
				<FormLabel>Location & Geo-coverage</FormLabel>
				<StyledTabs tabBarStyle={{ background: '#d7e6ee' }} onChange={handleTabChange} defaultActiveKey="1" type="card" size="large">
					<StyledTabPane tab="Countries" key="1">
						<StyledSelect
							$isMultiple
							allowClear
							mode="multiple"
							value={filterData.countries}
							onChange={handleChange}
							placeholder="Countries"
						>
							{options}
						</StyledSelect>
					</StyledTabPane>
					<StyledTabPane tab="Multi-Country" key="2">
						{renderMultiCountrySelector()}
					</StyledTabPane>
				</StyledTabs>
			</FormContainer>

		);
	}

	function renderMultiCountrySelector() {
		function generatePopoverContent(oraganizationName) {
			const organization = transnationalOrganizations.find(({ name }) => name === oraganizationName);
			const countryItems = organization.countryList.map(({ name, id }) => <div key={id}>{name}</div>);

			return (
				<PopoverContainer >
					{countryItems}
				</PopoverContainer>
			);
		}

		const options = facets.multiCountry.map(({ count, name }) => (
			<Option key={name}>
				{`${name} (${count})`}
				<Popover placement="right" content={generatePopoverContent(name)}>
					<StyledInfoIcon />
				</Popover>
			</Option>
		)).reverse();

		function handleChange(selectedList) {
			setSearchFilterData((prevState) => ({
				...prevState,
				multiCountry: selectedList,
			}))
		}

		return (
			<StyledSelect
				$isMultiple
				allowClear
				mode="multiple"
				value={filterData.multiCountry}
				onChange={handleChange}
				placeholder="Multi-Country"
			>
				{options}
			</StyledSelect>
		);
	}

	function renderTagSelect() {
		const options = facets.tags.map(({ count, name }) => <Option key={name}>{`${name} (${count})`}</Option>).reverse();
		function handleChange(selectedList) {
			setSearchFilterData((prevState) => ({
				...prevState,
				tags: selectedList,
			}))
		}

		return (
			<FormContainer>
				<FormLabel>Tags</FormLabel>
				<StyledSelect
					$isMultiple
					allowClear
					mode="multiple"
					value={filterData.tags}
					onChange={handleChange}
					placeholder="All (default)"
				>
					{options}
				</StyledSelect>
			</FormContainer>
		);
	}

	function renderThemeSelect() {
		function handleChange(selectedCategory) {
			setSearchFilterData((prevState) => ({
				...prevState,
				category: selectedCategory,
				subcategory: '',
			}))
		}

		function getCount(categoryName) {
			const foundCategroy = facets.categories.find(({ name }) => name === categoryName);

			return foundCategroy ? foundCategroy.count : 0;
		}

		return (
			<FormContainer>
				<Row>
					<FormLabel>Theme</FormLabel>
					<LabelDefaultValue>(All by default)</LabelDefaultValue>
				</Row>
				<ThemeList>
					{categories.map((category) => (
						<ThemeItem
							isSelected={category === filterData.category}
							key={category}
							title={category}
							icon={categoryIcons[category] ? categoryIcons[category] : './gov.png'}
							onSelect={handleChange}
							count={getCount(category)}
						/>
					))}
				</ThemeList>
			</FormContainer>
		);
	}

	function renderSubThemeSelect() {
		if (!filterData.category) {
			return null;
		}
		const options = facets.subcategories.map(({ count, name }) => <Option key={name}>{`${name} (${count})`}</Option>).reverse();

		function handleChange(selectedValue) {
			setSearchFilterData((prevState) => ({
				...prevState,
				subcategory: selectedValue ?? '',
			}))
		}

		return (
			<FormContainer>
				<FormLabel>Sub-Theme</FormLabel>
				<StyledSelect allowClear value={filterData.subcategory ? filterData.subcategory : null} placeholder="All (default)" onChange={handleChange}>
					{options}
				</StyledSelect>
			</FormContainer>
		);
	}

	function renderGoalSelect() {
		const options = facets.goals.map(({ count, name }) => <Option key={name}>{`${name} (${count})`}</Option>).reverse();
		function handleChange(selectedList) {
			setSearchFilterData((prevState) => ({
				...prevState,
				goals: selectedList,
			}))
		}

		return (
			<FormContainer>
				<FormLabel>Goals</FormLabel>
				<StyledSelect
					$isMultiple
					allowClear
					mode="multiple"
					value={filterData.goals}
					onChange={handleChange}
					placeholder="All (default)"
				>
					{options}
				</StyledSelect>
			</FormContainer>
		);
	}

	function renderFormatSelect() {
		const options = facets.formats.map(({ count, name }) => <Option key={name}>{`${name} (${count})`}</Option>).reverse();

		function handleChange(selectedValue) {
			setSearchFilterData((prevState) => ({
				...prevState,
				format: selectedValue ?? '',
			}))
		}

		return (
			<FormContainer>
				<FormLabel>Format</FormLabel>
				<StyledSelect allowClear value={filterData.format ? filterData.format : null} placeholder="All (default)" onChange={handleChange}>
					{options}
				</StyledSelect>
			</FormContainer>
		);
	}

	return (
		<Container>
			{renderThemeSelect()}
			{renderSubThemeSelect()}
			{renderCountrySelector()}
			{renderGoalSelect()}
			{renderTagSelect()}
			{renderFormatSelect()}
		</Container>
	);
};

export default FilterModal;
