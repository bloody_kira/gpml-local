import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
	height: 90px;
	width: 90px;
	border: 1px solid ${props => props.$isSelected ? '#05f081' : '#C4C4C4'};
	box-sizing: border-box;
	border-radius: 4px;
	margin: 5px;
	display: flex;
	justify-content: center;
	flex-direction: column;
	cursor: pointer;
`;

const IconContainer = styled.div`
	height: 50px;
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const Title = styled.div`
	font-style: normal;
	font-weight: normal;
	font-size: 9px;
	line-height: 10px;
	text-align: center;
	text-transform: uppercase;
	color: #BBBBBB;
`;

const Icon = styled.img`
	height: 35px;
`;

const Count = styled.div`
	font-style: normal;
	font-weight: bold;
	font-size: 12px;
	line-height: 11px;
	text-align: center;
	text-transform: uppercase;
	color: #C4C4C4;
	margin-bottom: 2px;
`;

const ThemeItem = ({ icon, title, count, isSelected, onSelect }) => {
	function handleSelect() {
		if (isSelected) {
			return onSelect('');
		}

		return onSelect(title);
	}



	return (
		<Container onClick={handleSelect} $isSelected={isSelected}>
			<IconContainer >
				<Icon src={icon} />
			</IconContainer>
			<Title>{title}</Title>
			{/* <Count>{count}</Count> */}
		</Container>
	);
};

export default ThemeItem;
