import React from 'react';
import styled from 'styled-components';
import { Tag } from 'antd';

const Container = styled.div`
	margin-top: 10px;
`;

const Label = styled.span`
	font-size: 15px;
	font-style: italic;
	color: white;
	margin-right: 10px;
`;

const Chips = styled(Tag)`
	color: white;
	border: white;
	background-color: #046799;
	border: 1px solid white;
	border-radius: 30px;
	font-size: 15px;

	& span {
		color: white;
		font-size: 13px;
	}

	&:hover span {
		color: #fefefe;
	}
`;

const Row = styled.div``;


const FilterIndicators = ({ searchFilterData, setSearchFilterData }) => {

	function handleRemoveSearchText() {
		setSearchFilterData((prevState) => ({
			...prevState,
			searchText: ''
		}));
	}

	function renderSearchText() {
		if (searchFilterData.searchText) {
			return (
				<Row>
					<Label>Search text:</Label>
					<Chips closable onClose={handleRemoveSearchText}>
						{searchFilterData.searchText}
					</Chips>
				</Row>
			);
		}
	}

	function renderCountries() {
		function handleRemove(countryName) {
			setSearchFilterData((prevState) => ({
				...prevState,
				countries: prevState.countries.filter((item) => item !== countryName)
			}));
		}

		if (searchFilterData.countries?.length) {
			const countryChipList = searchFilterData.countries.map((countryName) => (
				<Chips key={countryName} closable onClose={() => handleRemove(countryName)}>
					{countryName}
				</Chips>
			));
			return (
				<Row>
					<Label>Selected countries:</Label>
					{countryChipList}
				</Row>
			);
		}
	}

	function renderMultiCountries() {
		function handleRemove(multiCountryName) {
			setSearchFilterData((prevState) => ({
				...prevState,
				multiCountry: prevState.multiCountry.filter((item) => item !== multiCountryName)
			}));
		}

		if (searchFilterData.multiCountry?.length) {
			const multiCountryChipList = searchFilterData.multiCountry.map((multiCountryName) => (
				<Chips key={multiCountryName} closable onClose={() => handleRemove(multiCountryName)}>
					{multiCountryName}
				</Chips>
			));

			return (
				<Row>
					<Label>Selected Multi-Countries:</Label>
					{multiCountryChipList}
				</Row>
			);
		}
	}

	function renderGoals() {
		function handleRemove(goalName) {
			setSearchFilterData((prevState) => ({
				...prevState,
				goals: prevState.goals.filter((item) => item !== goalName)
			}));
		}

		if (searchFilterData.goals?.length) {
			const goalChipList = searchFilterData.goals.map((goalName) => (
				<Chips key={goalName} closable onClose={() => handleRemove(goalName)}>
					{goalName}
				</Chips>
			));

			return (
				<Row>
					<Label>Selected goals:</Label>
					{goalChipList}
				</Row>
			);
		}
	}

	function renderTags() {
		function handleRemove(tagName) {
			setSearchFilterData((prevState) => ({
				...prevState,
				tags: prevState.tags.filter((item) => item !== tagName)
			}));
		}

		if (searchFilterData.tags?.length) {
			const countryChipList = searchFilterData.tags.map((tagName) => (
				<Chips key={tagName} closable onClose={() => handleRemove(tagName)}>
					{tagName}
				</Chips>
			));

			return (
				<Row>
					<Label>Selected tags:</Label>
					{countryChipList}
				</Row>
			);
		}
	}
	
	function renderFormat() {
		function handleRemove() {
			setSearchFilterData((prevState) => ({
				...prevState,
				format: ''
			}));
		}

		if (searchFilterData.format) {
			const formatChip = (
				<Chips closable onClose={handleRemove}>
					{searchFilterData.format}
				</Chips>
			);

			return (
				<Row>
					<Label>Selected format:</Label>
					{formatChip}
				</Row>
			);
		}
	}

	function renderTheme() {
		function handleRemove() {
			setSearchFilterData((prevState) => ({
				...prevState,
				category: '',
				subcategory: '',
			}));
		}

		if (searchFilterData.category) {
			const formatChip = (
				<Chips closable onClose={handleRemove}>
					{searchFilterData.category}
				</Chips>
			);

			return (
				<Row>
					<Label>Selected Theme:</Label>
					{formatChip}
				</Row>
			);
		}
	}

	function renderSubtheme() {
		function handleRemove() {
			setSearchFilterData((prevState) => ({
				...prevState,
				subcategory: ''
			}));
		}

		if (searchFilterData.subcategory) {
			const formatChip = (
				<Chips closable onClose={handleRemove}>
					{searchFilterData.subcategory}
				</Chips>
			);

			return (
				<Row>
					<Label>Selected Sub-Theme:</Label>
					{formatChip}
				</Row>
			);
		}
	}

	return (
		<Container className="container">
			{renderSearchText()}
			{renderTheme()}
			{renderSubtheme()}
			{renderCountries()}
			{renderMultiCountries()}
			{renderGoals()}
			{renderTags()}
			{renderFormat()}
		</Container>
	);
};

export default FilterIndicators;
