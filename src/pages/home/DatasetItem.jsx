import React from 'react';
import styled from 'styled-components';
// import convertHtmlToReact from '@hedgedoc/html-to-react';
import dayjs from 'dayjs';

const LearnMore = styled.div`
	color: #01abf1;
	display: none;
	position: absolute;
	right: 10px;
	bottom: 10px;
`;

const Container = styled.div`
	width: 100%;
	max-height: 250px;
	overflow: hidden;
	background: #fff;
	box-shadow: 1px 1px 5px lightgrey;
	padding: 25px 20px 35px 20px;
	border: 1px solid lightgrey;
	border-radius: 4px;
	cursor: pointer;
	border-bottom: 1px solid #fff;
	position: relative;
	margin-bottom: 12px;

	&: hover {
		border-bottom: 1px solid #01abf1;
	}

	&:hover ${LearnMore} {
		display: inline-block;
	}
`;

const Title = styled.h4`
	-webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    --antd-wave-shadow-color: #1890ff;
    --scroll-bar: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Open Sans","Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
    -webkit-font-smoothing: antialiased;
    font-variant: tabular-nums;
    line-height: 1.5715;
    list-style: none;
    font-feature-settings: "tnum","tnum";
    margin-top: 0;
    margin-bottom: .5em;
    color: rgba(0,0,0,.85);
    font-weight: 500;
    font-size: 2rem;
    box-sizing: border-box;
    width: 85%;
`;

const Subtitle = styled.span`
	display: block;
	opacity: .75;
	margin-right: 10px;
	font-size: 12px;
	font-weight: 500;
	margin-bottom: 1em;
`;

const Content = styled.div`
	-webkit-text-size-adjust: 100%;
	-webkit-tap-highlight-color: rgba(0,0,0,0);
	--antd-wave-shadow-color: #1890ff;
	--scroll-bar: 0;
	font-family: -apple-system,BlinkMacSystemFont,"Open Sans","Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
	-webkit-font-smoothing: antialiased;
	color: rgba(0,0,0,.85);
	font-size: 14px;
	font-variant: tabular-nums;
	line-height: 1.5715;
	list-style: none;
	font-feature-settings: "tnum","tnum";
	box-sizing: border-box;

	display: -webkit-box;
	-webkit-line-clamp: 3;
	-webkit-box-orient: vertical;  
	overflow: hidden;
`;


const DatasetItem = ({ title, notes, url, category, metadata_modified }) => {
	function getFormattedUrl () {
		if (!url.match(/^http?:\/\//i) && !url.match(/^https?:\/\//i)) {
			return 'http://' + url;
		}

		return url;
	}

	function getFormattedDate() {
		return dayjs(new Date(metadata_modified)).format('MM/DD/YYYY')
	}

	function handleOpenDatasetInGpmlPlatform() {
		window.location = getFormattedUrl();
	}

	return (
		<Container onClick={handleOpenDatasetInGpmlPlatform}>
			<Title>{title}</Title>
			<Subtitle>Data category: {category}</Subtitle>
			<Subtitle>Last Modified: {getFormattedDate()}</Subtitle>
			{/* <Subtitle>Country: {countries.value.split(',').map((item) => item.trim()).map((country) => country).join(' | ')}</Subtitle> */}
			<Content dangerouslySetInnerHTML={{__html: notes}}></Content>
			<LearnMore>
				Read more &#10230;
			</LearnMore>
		</Container>
	);
};

export default DatasetItem;
