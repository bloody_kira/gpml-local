import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Input, Select, DatePicker, Radio, Space, Popover, Form, message } from 'antd';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Formik } from 'formik';
import TopBar from './TopBar';
import { categories, subCategories, geoCoverage, dataType as dataTypeList, format as formatList, transnationalOrganizations, membersList } from '../../constants/dataSetForm';
import { getTagList, getCountryList, getGoalList, getTransnationalList, getLanguageList, createDataset, getLicenseList } from '../../api';
import { isValidUrl } from '../../helpers/validation';

const { Option } = Select;


const Container = styled.div`
	padding-bottom: 110px;
`;
const FormWrapper = styled.div``;

const FormContainer = styled.div`
	margin: 25px 0px;
`;

const FormLabel = styled.span`
	font-style: normal;
	font-weight: normal;
	font-size: 18px;
	margin-bottom: 4px;
	display: block;
	line-height: 25px;
	color: #000000;
`;

const StyledInput = styled(Input)`
	background: #EDF2F7;

	&::placeholder {
		color: #A5B0C9;
	}

	& .ant-input {
		background: #EDF2F7;
	}
`;

const RowDiv = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
`;

const StyledSelect = styled(Select)`
	width: 100%;
	border-radius: 8px;

	& .ant-select-selector {
		background: ${props => props.$isMultiple ? '#EDF2F7' : '#FFF'} !important;
	}

	& .ant-select-selection-placeholder {
		color: #A5B0C9 !important;
	}

	& .ant-select-selection-item {
		background-color: white !important;
	}
`;

const StyledDatePicker = styled(DatePicker.RangePicker)`
	width: 100%;
	background: #EDF2F7;

	& input {
		&::placeholder {
			color: #A5B0C9;
		}
	}
`;

const RichTextEditor = styled(ReactQuill)`
	height: 200px;
	margin-bottom: 50px;
`;

const PopoverContainer = styled.div`
	max-height: 50vh;
    padding: 0 12px;
	width: 280px;
    overflow: auto;

	/* width */
	&::-webkit-scrollbar {
		width: 3px;
	}

	/* Track */
	&::-webkit-scrollbar-track {
		background: #f1f1f1; 
	}
	
	/* Handle */
	&::-webkit-scrollbar-thumb {
		background: #346896; 
	}

	/* Handle on hover */
	&::-webkit-scrollbar-thumb:hover {
		background: #1890ff;
	}
`;

const StyledInfoIcon = styled(InfoCircleOutlined)`
	color: #036799;
	margin-left: 5px;
`;

const RequiredFieldMessage = styled.span`
	font-style: normal;
	font-weight: normal;
	font-size: 17px;
	margin-left: 5px;
	color: #ED4337;
	display: block;
	line-height: 25px;
`;

const CreateDatasetPage = () => {
	const [tagList, setTagList] = useState([]);
	const [vocabulary, setVocabulary] = useState({
		countryList: [],
		goalList: [],
		languageList: [],
		transnationalOrgsList: [],
		licenseList: [],
	});
	const [requiredFieldsHighlited, setRequiredFieldsHighlited] = useState(false);

	useEffect(() => {
		fetchVocabularyAndTags();
	}, []);

	async function fetchVocabularyAndTags() {
		const tagList = await getTagList();
		const countryList = await getCountryList();
		const goalList = await getGoalList();
		const languageList = await getLanguageList();
		const transnationalOrgsList = await getTransnationalList();
		const licenseList = await getLicenseList();
		const filteredTagList = tagList.filter((tagName) => !countryList.includes(tagName) && !goalList.includes(tagName) && !languageList.includes(tagName) && !transnationalOrgsList.includes(tagName));

		setTagList(Array.from(new Set(filteredTagList)));
		setVocabulary({
			countryList: Array.from(new Set(countryList)),
			goalList: Array.from(new Set(goalList)),
			languageList: Array.from(new Set(languageList)),
			transnationalOrgsList: Array.from(new Set(transnationalOrgsList)),
			licenseList
		});
	}

	function getCategoryOption() {
		const options = categories.map((category) => <Option key={category}>{category}</Option>);
		return options;
	}

	function getSubCategoryOptions(category) {
		if (category) {
			const options = subCategories[category].map((subCategory) => <Option key={subCategory}>{subCategory}</Option>);

			return options;
		}

		return [];
	}

	function getTagOptionList() {
		const options = tagList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function getGoalsOptionList() {
		const options = vocabulary.goalList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function getLanguagesOptionList() {
		const options = vocabulary.languageList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function getFormatOptionList() {
		const options = formatList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function getDatatypeOptionList() {
		const options = dataTypeList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function getLicenseOptionList() {
		const options = vocabulary.licenseList.map((item) => <Option key={item.id}>{item.title}</Option>);
		return options;
	}

	function getMembersOptionList() {
		const options = membersList.map((item) => <Option key={item}>{item}</Option>);
		return options;
	}

	function handleValidate(values) {
		const errors = {};

		if (!values.title) {
			errors.title = 'Required';
		}

		if (!values.owners.length) {
			errors.owners = 'Required';
		}

		if (!values.partners.length) {
			errors.partners = 'Required';
		}

		if (!values.sponsors.length) {
			errors.sponsors = 'Required';
		}

		if (!values.dataType) {
			errors.dataType = 'Required';
		}

		if (!values.format) {
			errors.format = 'Required';
		}

		if (!values.license) {
			errors.license = 'Required';
		}

		if (!values.category) {
			errors.category = 'Required';
		}

		if (!values.subCategory) {
			errors.subCategory = 'Required';
		}

		if (!values.description) {
			errors.description = 'Required';
		}

		if (!values.dataSource) {
			errors.dataSource = 'Required';
		}

		if (values.dataSource && !isValidUrl(values.dataSource)) {
			errors.dataSource = 'Should be valid url';
		}

		if (!values.tags.length) {
			errors.tags = 'At leaset one tag should be selected';
		}

		if (!values.languages.length) {
			errors.languages = 'At leaset one language should be selected';
		}

		if (!values.goals.length) {
			errors.goals = 'At leaset one goal should be selected';
		}

		if (!values.startEndDate.length) {
			errors.startEndDate = 'Required';
		}

		if (values.geoCoverageType === geoCoverage.transnational) {
			if (!values.transnationals.length) {
				errors.transnationals = 'At leaset one transnational organization should be selected';
			}
		}

		if (values.geoCoverageType === geoCoverage.national) {
			if (!values.countries.length) {
				errors.countries = 'At leaset one country organization should be selected';
			}
		}

		if (values.geoCoverageType === geoCoverage.subnational) {
			if (!values.country) {
				errors.country = 'Required';
			}
			if (!values.countryArea) {
				errors.countryArea = 'Required';
			}
		}

		return errors;
	};

	function renderMultiCountrySelector(values, setFieldValue, touched, errors) {
		function generatePopoverContent(oraganizationName) {
			const organization = transnationalOrganizations.find(({ name }) => name === oraganizationName);

			const countryItems = organization ? organization.countryList.map(({ name, id }) => <div key={id}>{name}</div>) : null;

			return (
				<PopoverContainer >
					{countryItems}
				</PopoverContainer>
			);
		}

		const options = vocabulary.transnationalOrgsList.map((name) => (
			<Option key={name}>
				{name}
				<Popover placement="right" content={generatePopoverContent(name)}>
					<StyledInfoIcon />
				</Popover>
			</Option>
		));

		const countryOptions = vocabulary.countryList.map((countryName) => <Option key={countryName}>{countryName}</Option>);

		return (
			<>
				<FormContainer>
					<RowDiv><FormLabel>Transnational organizations</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
					<Form.Item
						validateStatus={touched.transnationals && errors.transnationals ? 'error' : 'validating'}
						help={touched.transnationals ? errors.transnationals : ''}
					>
						<StyledSelect
							$isMultiple
							allowClear
							mode="multiple"
							value={values.transnationals}
							onChange={(data) => setFieldValue('transnationals', data)}
							placeholder="Transnational organizations"
						>
							{options}
						</StyledSelect>
					</Form.Item>
				</FormContainer>
				<FormContainer>
					<RowDiv><FormLabel>Countries</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
					<Form.Item
						validateStatus={touched.countries && errors.countries ? 'error' : 'validating'}
						help={touched.countries ? errors.countries : ''}
					>
						<StyledSelect
							$isMultiple
							allowClear
							mode="multiple"
							placeholder="Select countries"
							value={values.countries}
							onChange={(data) => setFieldValue('countries', data)}
						>
							{countryOptions}
						</StyledSelect>
					</Form.Item>
				</FormContainer>
			</>
		);
	}

	function renderGeoCoverageSubForm(geoCoverageType, values, setFieldValue, handleChange, touched, errors) {
		if (geoCoverageType === geoCoverage.global) {
			return null;
		}

		if (geoCoverageType === geoCoverage.transnational) {
			return renderMultiCountrySelector(values, setFieldValue, touched, errors);
		}

		if (geoCoverageType === geoCoverage.national) {
			const options = vocabulary.countryList.map((countryName) => <Option key={countryName}>{countryName}</Option>);

			return (
				<FormContainer>
					<RowDiv><FormLabel>Countries</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
					<Form.Item
						validateStatus={touched.countries && errors.countries ? 'error' : 'validating'}
						help={touched.countries ? errors.countries : ''}
					>
						<StyledSelect
							$isMultiple
							allowClear
							mode="multiple"
							placeholder="Select countries"
							value={values.countries}
							onChange={(data) => setFieldValue('countries', data)}
						>
							{options}
						</StyledSelect>
					</Form.Item>
				</FormContainer>
			);
		}

		if (geoCoverageType === geoCoverage.subnational) {
			const options = vocabulary.countryList.map((countryName) => <Option key={countryName}>{countryName}</Option>);

			return (
				<>
					<FormContainer>
						<RowDiv><FormLabel>Subnational country</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
						<Form.Item
							validateStatus={touched.country && errors.country ? 'error' : 'validating'}
							help={touched.country ? errors.country : ''}
						>
							<StyledSelect
								onChange={(data) => setFieldValue('country', data)}
								size="large"
								allowClear
								placeholder="Select a country"
							>
								{options}
							</StyledSelect>
						</Form.Item>
					</FormContainer>
					<FormContainer>
						<RowDiv><FormLabel>Subnational Area</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
						<Form.Item
							validateStatus={touched.countryArea && errors.countryArea ? 'error' : 'validating'}
							help={touched.countryArea ? errors.countryArea : ''}
						>
							<StyledInput name="countryArea" onChange={handleChange} placeholder="Area" size="large" />
						</Form.Item>
					</FormContainer>
				</>

			);
		}
	}

	return (
		<Container>
			<Formik
				initialValues={{
					title: '', category: '', subCategory: '', description: '', dataSource: '', tags: [], goals: [], geoCoverageType: geoCoverage.global, transnationals: [],
					owners: [], partners: [], sponsors: [], dataType: '', format: '', languages: [], info: '', startEndDate: [], countries: [], country: '', countryArea: '', license: '',
				}}
				validate={handleValidate}
				onSubmit={async (values, { setSubmitting }) => {
					const result = await createDataset({ ...values });

					message.success('You have successully added content to datahub');
					setTimeout(() => {
						window.location = '/';
					}, 3000);
				}}
			>
				{({
					values,
					setFieldValue,
					errors,
					touched,
					handleChange,
					handleSubmit,
					isSubmitting,
				}) => (
					<div>
						<TopBar
							onSubmit={handleSubmit}
							requiredFieldsHighlited={requiredFieldsHighlited}
							toggleRequiredFieldsHighlite={setRequiredFieldsHighlited}
						/>
						<FormWrapper className="container">
							<FormContainer>
								<RowDiv><FormLabel>Title</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.title && errors.title ? 'error' : 'validating'}
									help={touched.title ? errors.title : ''}
								>
									<StyledInput name="title" onChange={handleChange} value={values.title} placeholder="Title" size="large" />
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Data Category</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.category && errors.category ? 'error' : 'validating'}
									help={touched.category ? errors.category : ''}
								>
									<StyledSelect placeholder="Select a category" value={values.category} size="large" onChange={(data) => {
										setFieldValue('subCategory', '')
										setFieldValue('category', data)
									}}
									>
										{getCategoryOption()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Sub Category</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.subCategory && errors.subCategory ? 'error' : 'validating'}
									help={touched.subCategory ? errors.subCategory : ''}
								>
									<StyledSelect placeholder="Select a sub category" value={values.subCategory} size="large" onChange={(data) => setFieldValue('subCategory', data)}>
										{getSubCategoryOptions(values.category)}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Description</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.description && errors.description ? 'error' : 'validating'}
									help={touched.description ? errors.description : ''}
								>
									<RichTextEditor value={values.description} theme="snow" onChange={(data) => setFieldValue('description', data)} />
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Data Source</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.dataSource && errors.dataSource ? 'error' : 'validating'}
									help={touched.dataSource ? errors.dataSource : ''}
								>
									<StyledInput addonBefore="http://" value={values.dataSource} name="dataSource" size="large" placeholder="www.url.com" onChange={handleChange} />
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<FormLabel>Geo Coverage</FormLabel>
								<Radio.Group name="geoCoverageType" value={values.geoCoverageType} onChange={handleChange}>
									<Space direction="vertical">
										{Object.values(geoCoverage).map((item) => <Radio key={item} value={item}>{item}</Radio>)}
									</Space>
								</Radio.Group>
							</FormContainer>
							{renderGeoCoverageSubForm(values.geoCoverageType, values, setFieldValue, handleChange, touched, errors)}
							<FormContainer>
								<RowDiv><FormLabel>Tags</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.tags && errors.tags ? 'error' : 'validating'}
									help={touched.tags ? errors.tags : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.tags}
										onChange={(data) => setFieldValue('tags', data)}
										placeholder="Select tags"
										size="large"
									>
										{getTagOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Goals</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.goals && errors.goals ? 'error' : 'validating'}
									help={touched.goals ? errors.goals : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.goals}
										onChange={(data) => setFieldValue('goals', data)}
										placeholder="Select goals"
										size="large"
									>
										{getGoalsOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Owners</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.owners && errors.owners ? 'error' : 'validating'}
									help={touched.owners ? errors.owners : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.owners}
										onChange={(data) => setFieldValue('owners', data)}
										placeholder="Select owners"
										size="large"
									>
										{getMembersOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Partners</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.partners && errors.partners ? 'error' : 'validating'}
									help={touched.partners ? errors.partners : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.partners}
										onChange={(data) => setFieldValue('partners', data)}
										placeholder="Select partners"
										size="large"
									>
										{getMembersOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Sponsors/Donors</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.sponsors && errors.sponsors ? 'error' : 'validating'}
									help={touched.sponsors ? errors.sponsors : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.sponsors}
										onChange={(data) => setFieldValue('sponsors', data)}
										placeholder="Select partners"
										size="large"
									>
										{getMembersOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Data Type</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.dataType && errors.dataType ? 'error' : 'validating'}
									help={touched.dataType ? errors.dataType : ''}
								>
									<StyledSelect value={values.dataType} placeholder="Select type of data" size="large" onChange={(data) => setFieldValue('dataType', data)}>
										{getDatatypeOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Format</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.format && errors.format ? 'error' : 'validating'}
									help={touched.format ? errors.format : ''}
								>
									<StyledSelect value={values.format} placeholder="Select format" size="large" onChange={(data) => setFieldValue('format', data)}>
										{getFormatOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Languages</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.languages && errors.languages ? 'error' : 'validating'}
									help={touched.languages ? errors.languages : ''}
								>
									<StyledSelect
										$isMultiple
										allowClear
										mode="multiple"
										value={values.languages}
										onChange={(data) => setFieldValue('languages', data)}
										placeholder="Select languages"
										size="large"
									>
										{getLanguagesOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>Start and End Dates</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.startEndDate && errors.startEndDate ? 'error' : 'validating'}
									help={touched.languastartEndDateges ? errors.startEndDate : ''}
								>
									<StyledDatePicker onChange={(data, dateStrings) => setFieldValue('startEndDate', dateStrings)} size="large" />
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<RowDiv><FormLabel>License</FormLabel>{requiredFieldsHighlited && <RequiredFieldMessage>*</RequiredFieldMessage>}</RowDiv>
								<Form.Item
									validateStatus={touched.license && errors.license ? 'error' : 'validating'}
									help={touched.license ? errors.license : ''}
								>
									<StyledSelect value={values.license} placeholder="Select license type" size="large" onChange={(data) => setFieldValue('license', data)}>
										{getLicenseOptionList()}
									</StyledSelect>
								</Form.Item>
							</FormContainer>
							<FormContainer>
								<FormLabel>Info and Docs</FormLabel>
								<RichTextEditor value={values.info} theme="snow" onChange={(data) => setFieldValue('info', data)} />
							</FormContainer>
						</FormWrapper>
					</div>
				)}
			</Formik>
		</Container>
	);
};

export default CreateDatasetPage;
