import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Content from './Content';
import SearchBar from './SearchFilterBar';
import { getDatasetForAdminPanel, isCurrentUserAdmin } from './../../api';
import { dataSetPublishedStatuses } from '../../constants/sorting';

const Container = styled.div``;

const AdminPage = () => {
	const [datasetList, setDatasetList] = useState(null);
	const [selectedFilterOption, setSelectedFilterOption] = useState(dataSetPublishedStatuses.ALL);

	useEffect(() => {
		isCurrentUserAdmin().then((isAdmin) => {
			if (!isAdmin) {
				return window.location = '/';
			}
		});
		fetchFilteredDataList();
	}, []);

	async function fetchFilteredDataList(searchText = '') {
		setDatasetList(null);
		const data = await getDatasetForAdminPanel({ searchText });

		setDatasetList(data.result.results);
	}

	function getFilteredDataset() {
		if (selectedFilterOption === dataSetPublishedStatuses.ALL) {
			return datasetList;
		}

		if (selectedFilterOption === dataSetPublishedStatuses.PUBLISHED) {
			return datasetList.filter((item) => item.private === false && item.approval_status !== "declined");
		}

		if (selectedFilterOption === dataSetPublishedStatuses.DECLINED) {
			return datasetList.filter((item) => item.approval_status === "declined");
		}

		if (selectedFilterOption === dataSetPublishedStatuses.PENDING) {
			return datasetList.filter((item) => item.private === true && item.approval_status === "");
		}
	}

	return (
		<Container>
			<SearchBar
				onSearch={fetchFilteredDataList}
				selectedFilterOption={selectedFilterOption}
				setSelectedFilterOption={setSelectedFilterOption}
			/>
			<Content
				datasetList={getFilteredDataset()}
				fetchFilteredDataList={fetchFilteredDataList}
			/>
		</Container>
	);
};

export default AdminPage;
