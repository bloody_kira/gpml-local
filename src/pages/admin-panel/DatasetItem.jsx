import React from 'react';
import styled from 'styled-components';
import { Collapse, Button, Popconfirm } from 'antd';
import { approveDatasetPublication, declineDatasetPublication } from '../../api';

const { Panel } = Collapse;

const Container = styled.div`
	width: 100%;
	background: #fff;
	box-shadow: 1px 1px 5px lightgrey;
	padding: 15px 20px 15px 20px;
	border: 1px solid lightgrey;
	border-radius: 4px;
	cursor: pointer;
	border-bottom: 1px solid #fff;
	position: relative;
	margin-bottom: 12px;
`;

const Title = styled.h4`
	-webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    --antd-wave-shadow-color: #1890ff;
    --scroll-bar: 0;
    font-family: -apple-system,BlinkMacSystemFont,"Open Sans","Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
    -webkit-font-smoothing: antialiased;
    font-variant: tabular-nums;
    line-height: 1.5715;
    list-style: none;
    font-feature-settings: "tnum","tnum";
    margin-top: 0;
    margin-bottom: .5em;
    color: rgba(0,0,0,.85);
    font-weight: 500;
    font-size: 2rem;
    box-sizing: border-box;
    flex: 1;
`;

const Subtitle = styled.span`
	display: block;
	opacity: .75;
	margin-right: 10px;
	font-size: 12px;
	font-weight: 500;
	margin-bottom: 2em;
`;

const Row = styled.div`
	display: flex;
	align-items: center;
`;


const Status = styled.div`
	font-size: 15px;
	font-weight: 500;
	margin-right: 15px;
	font-style: italic;
	opacity: .75;
`;

const ApproveButton = styled(Button)`
	color: blue !important;
	border-color: blue !important;
`;

const DeclineButton = styled(Button)`
	color: red;
`;

const StyledCollapse = styled(Collapse)`
	background-color: white !important;

	& .ant-collapse-item {
		border: none !important;
	}
`;

const Label = styled.div`
	font-size: 14px;
	margin-right: 15px;
	opacity: .75;
`;

const Value = styled.div`
	font-size: 14px;
	margin-right: 15px;
	font-weight: 500;
	font-style: italic;
	color: black;
	flex: 1;
`;


const DatasetItem = ({ fetchFilteredDataList, ...datasetItem }) => {
	async function handleApprove() {
		await approveDatasetPublication(datasetItem.id);
		fetchFilteredDataList();
	}

	async function handleDecline() {
		await declineDatasetPublication(datasetItem.id);
		fetchFilteredDataList();
	}

	function renderActionButtonsOrStatus() {
		if (datasetItem.approval_status === 'declined') {
			return <Status>Declined</Status>
		}
		else if (!datasetItem.private) {
			return (<Status>Published</Status>);
		} else {
			return (
				<div>
					<ApproveButton ghost shape="round" onClick={handleApprove}>Publish</ApproveButton>
					<Popconfirm placement="bottomRight" title="Are you sure you want to decline? Declined datasets cannot be published." onConfirm={handleDecline} okText="Yes" cancelText="No">
						<DeclineButton type="text">Decline</DeclineButton>
					</Popconfirm>
				</div>
			);
		}
	}

	return (
		<Container>
			<Row>
				<Title>{datasetItem.title}</Title>
				{renderActionButtonsOrStatus()}
			</Row>
			<Subtitle>Data category: {datasetItem.category}</Subtitle>
			<StyledCollapse bordered={false} defaultActiveKey={['0']}>
				<Panel header="Show detail" key="1">
					<Row>
						<Label>Title:</Label>
						<Value>{datasetItem.title}</Value>
					</Row>
					<Row>
						<Label>Theme:</Label>
						<Value>{datasetItem.category}</Value>
					</Row>
					<Row>
						<Label>Sub-Theme:</Label>
						<Value>{datasetItem.sub_category}</Value>
					</Row>
					<Row>
						<Label>Url:</Label>
						<Value>{datasetItem.url}</Value>
					</Row>
					<Row>
						<Label>Geo Coverage:</Label>
						<Value>{datasetItem.geo_coverage}</Value>
					</Row>
					{
						datasetItem.country_code &&
						<Row>
							<Label>Countries:</Label>
							<Value>{datasetItem.country_code}</Value>
						</Row>
					}
					{
						datasetItem.transnational &&
						<Row>
							<Label>Transnational organizations:</Label>
							<Value>{datasetItem.transnational}</Value>
						</Row>
					}
					{
						datasetItem.subnational_area &&
						<Row>
							<Label>Sub-national area:</Label>
							<Value>{datasetItem.subnational_area}</Value>
						</Row>
					}
					<Row>
						<Label>Goals:</Label>
						<Value>{datasetItem.sdg_goals}</Value>
					</Row>
					<Row>
						<Label>Owners:</Label>
						<Value>{datasetItem.owners}</Value>
					</Row>
					<Row>
						<Label>Partners:</Label>
						<Value>{datasetItem.partners}</Value>
					</Row>
					<Row>
						<Label>Sponsors:</Label>
						<Value>{datasetItem.sponsors}</Value>
					</Row>
					<Row>
						<Label>Start Date:</Label>
						<Value>{datasetItem.start_date1}</Value>
					</Row>
					<Row>
						<Label>End Date:</Label>
						<Value>{datasetItem.end_date1}</Value>
					</Row>
					<Row>
						<Label>Related source:</Label>
						<Value>{datasetItem.related_source}</Value>
					</Row>
					<Row>
						<Label>Languages:</Label>
						<Value>{datasetItem.lang}</Value>
					</Row>
					<Row>
						<Label>Tags:</Label>
						<Value>{datasetItem.tags.map(({ name }) => name).join(', ')}</Value>
					</Row>
					<Row>
						<Label>Data type:</Label>
						<Value>{datasetItem.data_type}</Value>
					</Row>
					<Row>
						<Label>Data format:</Label>
						<Value>{datasetItem.data_format}</Value>
					</Row>
					<Row>
						<Label>Notes:</Label>
						<div dangerouslySetInnerHTML={{__html: datasetItem.notes}}></div>
					</Row>
					<Row>
						<Label>Info:</Label>
						<div dangerouslySetInnerHTML={{__html: datasetItem.info}}></div>
					</Row>
				</Panel>
			</StyledCollapse>
		</Container>
	);
};

export default DatasetItem;

// "license_id": "other-open",
// "private": true,
// "approval_status": ""