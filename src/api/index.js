import { setSearchParams } from '../helpers/searchParams';
import { geoCoverage} from '../constants/dataSetForm';
import uuid from 'uuid-random';

// http://51.144.136.68:5000/dataset/

// const SERVER_URL = 'https://ckan:5000';
const SERVER_URL = 'https://apps.unep.org/data-catalog';

const MAIN_ORG = "primary_org";

function generateQueryString({ tags, countries, goals, format, category, subcategory, multiCountry }) {
	// if (tags.length && countries.length) {
	// 	console.log('we go this waay');
	// 	return `&fq=tags:(${tags.join(' OR ')}) AND country_code:(${countries.join(' OR ')})`;
	// } else if (tags.length) {
	// 	return `&fq=tags:(${tags.map((tag) => `"${tag}"`).join(' AND ')})`;
	// } else if (countries.length) {
	// 	return `&fq=country_code:(${countries.join(' OR ')})`;
	// }

	// fetch(`http://20.73.56.226:5000/api/3/action/member_create`, {
	// 	method: 'POST',
	// 	headers: {
	// 		Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDUwNDE0MjUsImp0aSI6IlcxSkRaQk50c2o2RFBMTzVySjZydy05bk11cUtvaWZHTmJJQV82bVRhckh1ei1obVlYR21pejQtSzZOdkdsa2pxNldMa0ROSW45TzdwTUFtIn0.G1sny4uflrVFiIbBiwYcZZ0MbuEu7C3hW6-YYzjQmw4',
	// 		'Content-Type': 'application/json',
	// 	},
    //     body: JSON.stringify({
	// 		"id": "primary_org",
	// 		object: 'prabumember',
    //         object_type: 'user',
    //         capacity: 'admin'
	// 	})
	// }).then((data) => data.json()).then((response) => console.log('response is ', response));

	let queryString = '';

	if (format) {
		queryString += `&fq=data_format:("${format}")`;
	}

	if (category) {
		queryString += `${ queryString ? ' AND ' : '&fq=' }category:("${category}")`;
	}

	if (subcategory) {
		queryString += `${ queryString ? ' AND ' : '&fq=' }sub_category:("${subcategory}")`;
	}

	if (tags.length ||countries.length || goals.length || multiCountry.length ) {
		queryString += `${ queryString ? ' AND ' : '&fq=' }tags:(${[...tags, ...countries, ...goals, ...multiCountry].map((tag) => `"${tag}"`).join(' AND ')})`;
	}

	return queryString;
}

export const searchDataset = async ({ searchText, tags, countries, sort, goals, format, category, subcategory, multiCountry }) => {
	setSearchParams({ searchText, tags, countries, sort, goals, format, category, subcategory, multiCountry });

	let queryString = `q=${searchText}${generateQueryString({ tags, countries, goals, format, category, subcategory, multiCountry })}&facet.field=["tags", "data_format", "category", "sub_category"]&facet.limit=1000&rows=1000&sort=${sort}`;

	const response = await fetch(`${SERVER_URL}/api/3/action/package_search?${queryString}`);

	return response.json();
};

export const getDatasetForAdminPanel = async ({ searchText }) => {
	let queryString = `q=${searchText}&rows=1000&`;
	const token = localStorage.getItem('token');

	const response = await fetch(`${SERVER_URL}/api/3/action/package_search?${queryString}&include_private=True`, { // &fq=NOT(approval_status:"declined")
		method: 'GET',
		headers: {
			Authorization: token,
			'Content-Type': 'application/json',
		},
	});

	return response.json();
};

export const createDataset = async ({ title, category, subCategory, description, dataSource, tags, goals, geoCoverageType, transnationals, owners,
	partners, sponsors, dataType, format, languages, info, startEndDate, countries, country, countryArea, license }) => {
	const country_code = (geoCoverageType === geoCoverage.national || geoCoverageType === geoCoverage.transnational) ? countries.join(',') : (geoCoverageType === geoCoverage.subnational) ? country : '';
	const token = localStorage.getItem('token');

	const response = await fetch(`${SERVER_URL}/api/3/action/package_create`, {
			method: 'POST',
			headers: {
				Authorization: token,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				"owner_org": MAIN_ORG,
				"name": uuid(),
				"title": title,
				"category": category,
				"sub_category": subCategory,
				"notes": description,
				"url": dataSource,
				"geo_coverage": geoCoverageType,
				"country_code": country_code,
				"transnational": geoCoverageType === geoCoverage.transnational ? transnationals.join(',') : '',
				"subnational_area": geoCoverageType === geoCoverage.subnational ? countryArea : '',
				"sdg_goals": goals.join(','),
				"owners": owners.join(','),
				"partners": partners.join(','),
				"sponsors": sponsors.join(','),
				"start_date1": startEndDate[0],
				"end_date1": startEndDate[1],
				"related_source": "Related Source1",
				"lang": languages.join(','),
				"tag_string": tags.join(','),
				"data_type": dataType,
				"data_format": format,
				"info": info,
				"license_id": license,
				"private": true,
				"approval_status": ""
			})
		});

		const data = await response.json();
		return data;
};

export const getMemberRoleList = async () => {
	const token = localStorage.getItem('token');
	const response = await fetch(`${SERVER_URL}/api/3/action/member_roles_list`, {
		method: 'GET',
		headers: {
			Authorization: token,
			'Content-Type': 'application/json',
		},
	});
	console.log('member roleList response ', response);
	const data = await response.json();
	console.log('member roleList data ', data);
	return data.result;
};

export const approveDatasetPublication = async (dataSetId) => {
	const token = localStorage.getItem('token');

	const response = await fetch(`${SERVER_URL}/api/3/action/package_patch`, {
		method: 'POST',
		headers: {
			Authorization: token,
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			private: false,
			id: dataSetId,
		}),
	});
	const data = await response.json();
};


export const declineDatasetPublication = async (dataSetId) => {
	const token = localStorage.getItem('token');

	const response = await fetch(`${SERVER_URL}/api/3/action/package_patch`, {
		method: 'POST',
		headers: {
			Authorization: token,
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			"approval_status": "declined",
			id: dataSetId,
		})
	});

	const data = await response.json();
};

export const getTagList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/tag_list`);
	const data = await response.json();
	return data.result;
};

export const getCountryList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/tag_list?vocabulary_id=country_listv3`);
	const data = await response.json();
	return data.result;
};

export const getGoalList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/tag_list?vocabulary_id=sdg_goals_list`);
	const data = await response.json();
	return data.result;
};

export const getTransnationalList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/tag_list?vocabulary_id=transnational_list`);
	const data = await response.json();
	return data.result;
};


export const getLanguageList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/tag_list?vocabulary_id=language_list`);
	const data = await response.json();
	return data.result;
};

export const getLicenseList = async () => {
	const response = await fetch(`${SERVER_URL}/api/3/action/license_list`);
	const data = await response.json();
	return data.result;
};

export const isCurrentUserAdmin = async () => {
	const username = localStorage.getItem('username');
	const token = localStorage.getItem('token');

	if (!username || !token) {
		return false;
	}

	try {
		const response = await fetch(`${SERVER_URL}/api/3/action/organization_list_for_user?id=${username}`, {
			method: 'GET',
			headers: {
				Authorization: token,
				'Content-Type': 'application/json',
			},
		});
	
		const data = await response.json();
	
		const mainOrg = data.result.find(({ name }) => name === MAIN_ORG);
	
		if (!mainOrg) {
			return false;
		}
	
		if (mainOrg.capacity === 'admin') {
			return true;
		}
	
		
	} catch (error) {
		return false;
	}
	return false;
	
};