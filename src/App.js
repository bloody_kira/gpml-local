import './index.css';

import Header from './components/layout/Header';
import HomePageContent from './pages/home';
import CreateDatasetPage from './pages/dataset-create';
import AdminPanel from './pages/admin-panel';
import AuthRedirect from './pages/AuthRedirect';
import styled from 'styled-components';

const Container = styled.div`
	flex: 1;
	height: 100%;
`;

function App() {
	if (window.location.pathname === '/') {
		return (
			<Container>
				<Header />
				<HomePageContent />
			</Container>
		);
	}

	if (window.location.pathname === '/add-data') {
		const token = localStorage.getItem('token');

		if (!token) {
			return window.location = '/';
		}

		return (
			<Container>
				<Header />
				<CreateDatasetPage />
			</Container>
		);
	}

	if (window.location.pathname === '/admin') {
		const token = localStorage.getItem('token');

		if (!token) {
			return window.location = '/';
		}

		return (
			<Container>
				<Header />
				<AdminPanel />
			</Container>
		);
	}

	if (window.location.pathname === '/oauth2/callback') {
		return (
			<Container>
				<AuthRedirect />
			</Container>
		);
	}

	if (window.location.pathname === '/user/logout') {
		return (
			<Container>
				<AuthRedirect />
			</Container>
		);
	}

	window.location = '/';
}

export default App;
