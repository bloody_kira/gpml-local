import React, { useEffect, useState } from 'react';
import { Menu, Dropdown } from 'antd';
import { MenuOutlined, PlusOutlined } from '@ant-design/icons';
import { isCurrentUserAdmin } from '../../api';
import '../../bootstrap/css/bootstrap-theme.css';
import '../../bootstrap/css/bootstrap.css';
import {
	Container, Navbar, ContentCenter, HamburgerButton, NavbarItem, NavbarDropdownItem, DropdownChevron, LogoContainer,
	FilledButton, MenuItem, FloatingButtonAnchor, AddContentButton, AuthButtonsContainer, StyledDrawer, SignButton, NavCollapsible
} from './style';

const CALLBACK_URL = escape('https://apps.unep.org/data-catalog/oauth2/callback')
const authUrl = `https://unep-gpml.eu.auth0.com/authorize?response_type=code&client_id=lmdxuDGdQjUsbLbMFpjDCulTP1w5Z4Gi&redirect_uri=${CALLBACK_URL}&scope=openid+profile+email&state=eyJjYW1lX2Zyb20iOiAiL2Rhc2hib2FyZCJ9`;

const navLinks = {
	overview: {
		key: 'overview',
		label: 'Overview',
		link: 'https://datahub.gpmarinelitter.org/pages/about',
	},
	mapAndLayer: {
		key: 'mapAndLayer',
		label: 'Map and Layer',
		link: 'https://datahub.gpmarinelitter.org/',
	},
	dataExplore: {
		key: 'dataExplore',
		label: 'Data Explore',
		link: '/'
	},
	apiExplore: {
		key: 'apiExplore',
		label: 'Api Explore',
		link: 'https://datahub.gpmarinelitter.org/pages/api-explore'

	},
	storyMap: {
		key: 'storyMap',
		label: 'Story Maps',
		link: 'https://datahub.gpmarinelitter.org/pages/story_map',
	},
	dataPartners: {
		key: 'dataPartners',
		label: 'Data Partners',
		link: 'https://datahub.gpmarinelitter.org/pages/partner',
	},
	joinDatahub: {
		key: 'joinDatahub',
		label: 'Join GPML Data Hub+',
		link: 'https://digital.gpmarinelitter.org/signup',
	},
	signIn: {
		key: 'signIn',
		label: 'Sign In',
		link: authUrl,
	},
	signOut: {
		key: 'signOut',
		label: 'Sign Out',
		link: `https://unep-gpml.eu.auth0.com/v2/logout?client_id=lmdxuDGdQjUsbLbMFpjDCulTP1w5Z4Gi&returnTo=${window.location.origin}/user/logout`,
	},
	adminPanel: {
		key: 'adminPanel',
		label: 'Admin Panel',
		link: '/admin',
	},
	addData: {
		key: 'addData',
		label: 'Add data',
		link: '/add-data',
	},
	dataCatalog: {
		key: 'dataCatalog',
		label: 'Data Catalog',
		link: '#',
	}
};


const Header = () => {
	const [floatingBottonPosition, setFloatingBottonPosition] = useState(null);
	const [isAdminUser, setIsAdminUser] = useState(false);
	const [isDrawerVisible, setIsDrawerVisible] = useState(false);

	useEffect(() => {
		const sortButton = document.getElementById('sort-section');
		if (sortButton) {
			const { right } = sortButton.getBoundingClientRect();
			setFloatingBottonPosition({ right: window.innerWidth - (right + 5), bottom: 40 });
		}

		isCurrentUserAdmin().then((isAdmin) => setIsAdminUser(isAdmin));
	}, []);

	function handleMenuItemClick({ key }) {
		const navItem =  Object.values(navLinks).find((item) => item.key === key);
		if (!navItem) return;
 
		window.location = navItem.link;
	}

	function toggleDrawerVisibility() {
		setIsDrawerVisible((state) => !state);
	}

	function renderFloatingButton() {
		const token = localStorage.getItem('token');

		if (floatingBottonPosition) {
			const href = token ? navLinks.addData.link : navLinks.signIn.link;

			return (
				<FloatingButtonAnchor href={href} right={floatingBottonPosition.right} bottom={floatingBottonPosition.bottom}>
					<AddContentButton type="button" className="btn"><PlusOutlined /> {navLinks.addData.label}</AddContentButton>
				</FloatingButtonAnchor>
			);
		}
	}

	function renderDrawer() {
		const token = localStorage.getItem('token');

		return (
			<StyledDrawer
				width="75%"
				title="Menu"
				placement="right"
				onClose={toggleDrawerVisibility}
				visible={isDrawerVisible}
			>
				<Menu
					onClick={handleMenuItemClick}
					style={{ width: "100%" }}
					mode="inline"
				>
					<Menu.Item key={navLinks.overview.key}>{navLinks.overview.label}</Menu.Item>
					<Menu.Item key={navLinks.mapAndLayer.key}>{navLinks.mapAndLayer.label}</Menu.Item>
					<Menu.SubMenu key={navLinks.dataCatalog.key} title={navLinks.dataCatalog.label}>
						<Menu.Item key={navLinks.dataExplore.key}>{navLinks.dataExplore.label}</Menu.Item>
						<Menu.Item key={navLinks.apiExplore.key}>{navLinks.apiExplore.label}</Menu.Item>
					</Menu.SubMenu>
					<Menu.Item key={navLinks.storyMap.key}>{navLinks.storyMap.label}</Menu.Item>
					<Menu.Item key={navLinks.dataPartners.key}>{navLinks.dataPartners.label}</Menu.Item>
					{
						token ? (
							<>
								{
									isAdminUser && <Menu.Item key={navLinks.adminPanel.key}>{navLinks.adminPanel.label}</Menu.Item>
								}
								<Menu.Item key={navLinks.signOut.key}>{navLinks.signOut.label}</Menu.Item>
							</>
						) : (
							<>
								<Menu.Item key={navLinks.joinDatahub.key}>{navLinks.joinDatahub.label}</Menu.Item>
								<Menu.Item key={navLinks.signIn.key}>{navLinks.signIn.label}</Menu.Item>
							</>
						)
					}
				</Menu>
			</StyledDrawer>
		);
	}

	function renderAuthButton() {
		const token = localStorage.getItem('token');

		if (token) {
			return (
				<AuthButtonsContainer>
					{
						isAdminUser && (
							<a href={navLinks.adminPanel.link} style={{ marginRight: '6px' }}>
								<SignButton type="button" className="btn">{navLinks.adminPanel.label}</SignButton>
							</a>
						)
					}
					<a href={navLinks.signOut.link}>
						<FilledButton type="button" className="btn">{navLinks.signOut.label}</FilledButton>
					</a>
				</AuthButtonsContainer>
			);
		}

		return (
			<AuthButtonsContainer>
				<a href={navLinks.joinDatahub.link}>
					<FilledButton type="button" className="btn">{navLinks.joinDatahub.label}</FilledButton>
				</a>
				<a href={navLinks.signIn.link}>
					<SignButton type="button" className="btn">{navLinks.signIn.label}</SignButton>
				</a>
			</AuthButtonsContainer>
		);
	}

	const exploreMenu = (
		<Menu onClick={handleMenuItemClick} trigger={["click"]}>
			<MenuItem key={navLinks.dataExplore.key} style={{ borderBottom: '1px solid #00aaf1' }}>
				{navLinks.dataExplore.label}
			</MenuItem>
			<MenuItem key={navLinks.apiExplore.key}>
				{navLinks.apiExplore.label}
			</MenuItem>
		</Menu>
	);

	return (
		<Container>
			{renderDrawer()}
			<Navbar className="container">
				<ContentCenter className="container-fluid">
					<ContentCenter className="container">
						<div className="navbar-header">
							<LogoContainer href="https://digital.gpmarinelitter.org/">
								<img src="https://unep-gpml.akvotest.org/static/media/GPML-logo-white.a6e9862a.png" alt="Logo" height="75" />
							</LogoContainer>
						</div>

						<NavCollapsible className="collapse navbar-collapse" id="navbar-collapse">
							<ul className="nav navbar-nav">
								<li >
									<NavbarItem href={navLinks.overview.link}>{navLinks.overview.label}</NavbarItem>
								</li>
								<li >
									<NavbarItem href={navLinks.mapAndLayer.link}>{navLinks.mapAndLayer.label}</NavbarItem>
								</li>
								<li>
									<Dropdown overlay={exploreMenu}>
										<NavbarDropdownItem href="/">
											{navLinks.dataCatalog.label}
											<DropdownChevron viewBox="64 64 896 896" focusable="false" data-icon="down" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M884 256h-75c-5.1 0-9.9 2.5-12.9 6.6L512 654.2 227.9 262.6c-3-4.1-7.8-6.6-12.9-6.6h-75c-6.5 0-10.3 7.4-6.5 12.7l352.6 486.1c12.8 17.6 39 17.6 51.7 0l352.6-486.1c3.9-5.3.1-12.7-6.4-12.7z"></path></DropdownChevron>
										</NavbarDropdownItem>
									</Dropdown>
								</li>
								<li>
									<NavbarItem href={navLinks.storyMap.link}>{navLinks.storyMap.label}</NavbarItem>
								</li>
								<li>
									<NavbarItem href={navLinks.dataPartners.link}>{navLinks.dataPartners.label}</NavbarItem>
								</li>
							</ul>
						</NavCollapsible>
					</ContentCenter>
					{renderAuthButton()}
					<HamburgerButton onClick={toggleDrawerVisibility} shape="circle" icon={<MenuOutlined />} />
				</ContentCenter>
				{renderFloatingButton()}
			</Navbar>
		</Container>
	);
};

export default Header;
